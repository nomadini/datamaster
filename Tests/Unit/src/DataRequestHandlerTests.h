/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef DataRequestHandlerTests_H
#define DataRequestHandlerTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "DataRequestHandler.h"
#include "MockHttpResponse.h"
#include "MockHttpRequest.h"

class DataRequestHandlerTests  : public ::testing::Test {

private:

public:
    DataRequestHandlerPtr dataRequestHandler;

    DataRequestHandlerTests();

    void thenResponseIsAsExpected(MockHttpResponse& response, std::string expectedResponseInJson);

    void SetUp();

    virtual ~DataRequestHandlerTests() ;

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_MODULES_TARGETGROUPDAILYCAPSFILTERMODULE_H_ */
