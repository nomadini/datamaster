
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "DataRequestHandlerTests.h"
#include "DataRequestHandler.h"
#include "DataMasterContext.h"
#include "TargetGroupTestHelper.h"
#include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include <boost/foreach.hpp>
#include "DataRequestHandler.h"
#include "MockHttpRequest.h"
#include "MockHttpResponse.h"


DataRequestHandlerTests::DataRequestHandlerTests() {

}

DataRequestHandlerTests::~DataRequestHandlerTests() {

}

void DataRequestHandlerTests::SetUp() {
        // AerospikeDriver* aeroSpikeDriver = std::make_shared<AerospikeDriver>("127.0.0.1", 3000, nullptr);
        // aeroSpikeDriver->entityToModuleStateStats = entityToModuleStateStats;
        //we use a mock here, to be able to verify the calls on the mock
        //the real json comparisons are done in the tests for each cache service ..eg CreativeCacheServiceTests.
        // AppMainProcessorPtr appMainProcessor = std::make_shared<AppMainProcessor>(
        //         beanFactory->advertiserCacheServiceMock,
        //         beanFactory->creativeCacheServiceMock,
        //         beanFactory->targetGroupCacheServiceMock,
        //         beanFactory->campaignCacheServiceMock,
        //         beanFactory->entityToModuleStateStats,
        //         beanFactory->targetGroupBWListCacheServiceMock,
        //         beanFactory->targetGroupCreativeCacheServiceMock,
        //         beanFactory->targetGroupGeoSegmentCacheServiceMock,
        //         beanFactory->targetGroupDayPartCacheServiceMock,
        //         beanFactory->targetGroupGeoLocationCacheServiceMock,
        //         beanFactory->entityDeliveryInfoCacheServiceMock,
        //         beanFactory->globalWhiteListedCacheServiceMock,
        //         aeroSpikeDriver,
        //         beanFactory->cassandraDriver);

        EntityToModuleStateStats* entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();
        dataRequestHandler = std::make_shared<DataRequestHandler>(entityToModuleStateStats);
}


void DataRequestHandlerTests::thenResponseIsAsExpected(MockHttpResponse& response,
                                                       std::string expectedResponseInJson) {
        auto response1 = response.getResponseBody();

}

TEST_F(DataRequestHandlerTests, testGettingCreativesInJson) {

        // ON_CALL(*beanFactory->creativeCacheServiceMock, processReadAll(_))
        //   .WillByDefault(Return("thisAllCreatives"));
        EXPECT_CALL(*beanFactory->creativeCacheServiceMock, processReadAll(_))
        .WillOnce(Return("thisAllCreatives"));
        std::string requestBody = "blah";
        MockHttpRequest request(requestBody);
        request.setURI ("?entity=creative&function=all");
        MockHttpResponse response;
        dataRequestHandler->handleRequest (request, response);
        EXPECT_THAT(response.getStatus(),
                    testing::Eq(Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK));
}

//fix this test later, for some reason it segfaults
// TEST_F(DataRequestHandlerTests , testGettingAdvertiserInJson) {
//
//     EXPECT_CALL(*beanFactory->advertiserCacheServiceMock, processReadAll(_))
//           .WillOnce(Return("thisAllAdvertisers"));
//     std::string requestBody = "blah";
//     MockHttpRequest request(requestBody);
//     request.setURI ("?entity=advertiser&function=all");
//     MockHttpResponse response;
//     dataRequestHandler->handleRequest (request, response);
//     EXPECT_THAT(response.getStatus(),
//                 testing::Eq(Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK));
// }

TEST_F(DataRequestHandlerTests, testGettingCampaignsInJson) {

        EXPECT_CALL(*beanFactory->campaignCacheServiceMock, processReadAll(_))
        .WillOnce(Return("thisAllCampaigns"));
        std::string requestBody = "blah";
        MockHttpRequest request(requestBody);
        request.setURI ("?entity=campaign&function=all");
        MockHttpResponse response;
        dataRequestHandler->handleRequest (request, response);
        EXPECT_THAT(response.getStatus(),
                    testing::Eq(Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK));
}

TEST_F(DataRequestHandlerTests, testGettingTargetGroupsInJson) {

        EXPECT_CALL(*beanFactory->targetGroupCacheServiceMock, processReadAll(_))
        .WillOnce(Return("thisAllTargetGroups"));
        std::string requestBody = "blah";
        MockHttpRequest request(requestBody);
        request.setURI ("?entity=targetgroup&function=all");
        MockHttpResponse response;
        dataRequestHandler->handleRequest (request, response);
        EXPECT_THAT(response.getStatus(),
                    testing::Eq(Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK));
}

TEST_F(DataRequestHandlerTests, testGettingTargetGroupBWListsInJson) {

        EXPECT_CALL(*beanFactory->targetGroupBWListCacheServiceMock, processReadAll(_))
        .WillOnce(Return("thisAllTargetGroupBWLists"));
        std::string requestBody = "blah";
        MockHttpRequest request(requestBody);
        request.setURI ("?entity=TargetGroupBWList&function=all");
        MockHttpResponse response;
        dataRequestHandler->handleRequest (request, response);
        EXPECT_THAT(response.getStatus(),
                    testing::Eq(Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK));
}

TEST_F(DataRequestHandlerTests, testGettingTargetGroupGeoSegmentsInJson) {

        EXPECT_CALL(*beanFactory->targetGroupGeoSegmentCacheServiceMock, processReadAll(_))
        .WillOnce(Return("thisAllTargetGroupGeoSegments"));
        std::string requestBody = "blah";
        MockHttpRequest request(requestBody);
        request.setURI ("?entity=TargetGroupGeoSegment&function=all");
        MockHttpResponse response;
        dataRequestHandler->handleRequest (request, response);
        EXPECT_THAT(response.getStatus(),
                    testing::Eq(Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK));
}

TEST_F(DataRequestHandlerTests, testGettingTargetGroupDayPartsInJson) {

        EXPECT_CALL(*beanFactory->targetGroupDayPartCacheServiceMock, processReadAll(_))
        .WillOnce(Return("thisAllTargetGroupDayParts"));
        std::string requestBody = "blah";
        MockHttpRequest request(requestBody);
        request.setURI ("?entity=TargetGroupDayPart&function=all");
        MockHttpResponse response;
        dataRequestHandler->handleRequest (request, response);
        EXPECT_THAT(response.getStatus(),
                    testing::Eq(Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK));
}

TEST_F(DataRequestHandlerTests, testGettingTargetGroupGeoLocationsInJson) {

        EXPECT_CALL(*beanFactory->targetGroupGeoLocationCacheServiceMock, processReadAll(_))
        .WillOnce(Return("thisAllTargetGroupGeoLocation"));
        std::string requestBody = "blah";
        MockHttpRequest request(requestBody);
        request.setURI ("?entity=TargetGroupGeoLocation&function=all");
        MockHttpResponse response;
        dataRequestHandler->handleRequest (request, response);
        EXPECT_THAT(response.getStatus(),
                    testing::Eq(Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK));
}

TEST_F(DataRequestHandlerTests, testGettingTgRealTimeDeliveryInfosInJson) {

        EXPECT_CALL(*beanFactory->entityDeliveryInfoCacheServiceMock, processReadAll(_))
        .WillOnce(Return("thisAllTgRealTimeDeliveryInfos"));
        std::string requestBody = "blah";
        MockHttpRequest request(requestBody);
        request.setURI ("?entity=EntityRealTimeDeliveryInfo&function=all");
        MockHttpResponse response;
        dataRequestHandler->handleRequest (request, response);
        EXPECT_THAT(response.getStatus(),
                    testing::Eq(Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK));
}
