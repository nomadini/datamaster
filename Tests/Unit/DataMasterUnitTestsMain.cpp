/*
 *
 *
 *  Created on: March 7, 2016
 *      Author: mtaabodi
 */

#include "GUtil.h"


#include "CassandraDriverInterface.h"
#include "ConfigService.h"
#include "DataMaster.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "TestsCommon.h"
#include "SignalHandler.h"
#include <string>
#include <memory>
#include "TempUtil.h"
#include "HttpUtil.h"

int runGoogleTests(int argc, char** argv) {
    // The following line must be executed to initialize Google Mock
    // (and Google Test) before running the tests.
    ::testing::InitGoogleMock(&argc, argv);
    ::testing::FLAGS_gmock_verbose ="info";

    return RUN_ALL_TESTS();
}


int main(int argc, char** argv) {

    google::InitGoogleLogging(*argv);
    FLAGS_logtostderr = true;
//    FLAGS_log_dir = "/some/log/directory";
//    FLAGS_stderrthreshold = 0;
    FLAGS_colorlogtostderr = true;
//    FLAGS_minloglevel = 0;
    FLAGS_v = 10;//print out all from 0 to 10 levels
//    FLAGS_v = 1;//print out all from 0 to 10 levels
    FLAGS_alsologtostderr = true;
    LogLevelManager::enableAll ();



    runGoogleTests(argc, argv);
//    CassandraDriver::closeSessionAndCluster();
    return 0;
}
