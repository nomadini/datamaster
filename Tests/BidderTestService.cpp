
#include "BidderTestService.h"
#include "GUtil.h"
#include "DeviceTestHelper.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "ConverterUtil.h"
#include "TargetGroupTestHelper.h"
#include "CampaignTestHelper.h"
#include "CreativeTestHelper.h"

BidderTestService::BidderTestService() {

        targetGroup = TargetGroupTestHelper::createSampleTargetGroup();
        MLOG(3)<<"tg json " << targetGroup->toJson();
        campaign = CampaignTestHelper::createSampleCampaign();
        creative = CreativeTestHelper::createSampleCreative ();

}
std::shared_ptr<EntityRealTimeDeliveryInfo> BidderTestService::createSampleTargetGroupRealtimInfo() {
        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeDeliveryInfo = std::make_shared<EntityRealTimeDeliveryInfo>();
        tgRealTimeDeliveryInfo->timeReported = DateTimeUtil::getCurrentDateWithSecondPercision ();
        tgRealTimeDeliveryInfo->targetGroupId = 12;

        tgRealTimeDeliveryInfo->numOfImpressionsServedInCurrentDateUpToNow = std::make_shared<gicapods::AtomicLong>(0);
        tgRealTimeDeliveryInfo->numOfImpressionsServedOverallUpToNow  = std::make_shared<gicapods::AtomicLong>(0);

        tgRealTimeDeliveryInfo->platformCostSpentInCurrentDateUpToNow = std::make_shared<gicapods::AtomicDouble>(0);
        tgRealTimeDeliveryInfo->platformCostSpentOverallUpToNow = std::make_shared<gicapods::AtomicDouble>(0);
        return tgRealTimeDeliveryInfo;
}

std::shared_ptr<CampaignRealTimeInfo> BidderTestService::createSampleCampaignRealtimInfo() {

        std::shared_ptr<CampaignRealTimeInfo> campaignRealTimeInfo = std::make_shared<CampaignRealTimeInfo>();
        campaignRealTimeInfo->timeReported = DateTimeUtil::getCurrentDateWithSecondPercision ();
        campaignRealTimeInfo->campaignId = 12;

        campaignRealTimeInfo->numOfImpressionsServedInCurrentDateUpToNow = std::make_shared<gicapods::AtomicLong>(0);
        campaignRealTimeInfo->numOfImpressionsServedOverallUpToNow  = std::make_shared<gicapods::AtomicLong>(0);

        campaignRealTimeInfo->platformCostSpentInCurrentDateUpToNow = std::make_shared<gicapods::AtomicDouble>(0);
        campaignRealTimeInfo->platformCostSpentOverallUpToNow = std::make_shared<gicapods::AtomicDouble>(0);

        // campaignRealTimeInfo->lastTimeAdShown = DateTimeUtil::getCurrentDateWithSecondPercision ();

        return campaignRealTimeInfo;
}
