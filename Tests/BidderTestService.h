#ifndef BidderTestService_H
#define BidderTestService_H


#include "DeviceTestHelper.h"
#include <memory>
#include <string>
#include "DataMasterContext.h"
class StringUtil;
#include "GeoSegment.h"
#include "GeoLocation.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CampaignTypeDefs.h"
#include "CreativeTypeDefs.h"
class Creative;
#include "EntityRealTimeDeliveryInfo.h"
#include "CampaignRealTimeInfo.h"

class BidderTestService;

class BidderTestService {

public:

BidderTestService();

std::shared_ptr<TargetGroup> targetGroup;
std::shared_ptr<DataMasterContext> context;
std::shared_ptr<Creative> creative;
std::shared_ptr<Campaign> campaign;
std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeInfo;
std::shared_ptr<CampaignRealTimeInfo> campaignRealTimeInfo;

std::shared_ptr<CampaignRealTimeInfo> createSampleCampaignRealtimInfo();
std::shared_ptr<EntityRealTimeDeliveryInfo> createSampleTargetGroupRealtimInfo();
};

#endif
