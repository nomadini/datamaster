#include "GUtil.h"
#include "Status.h"

#include "TargetGroup.h"
#include "Creative.h"

#include "AdHistory.h"
#include "Campaign.h"
#include "StringUtil.h"
#include "DataMasterContext.h"
#include "JsonUtil.h"
#include "ConverterUtil.h"
#include "DateTimeUtil.h"
#include "StringUtil.h"

DataMasterContext::DataMasterContext() {

}


std::string DataMasterContext::toString() {
    return this->toJson ();
}

std::shared_ptr<DataMasterContext> DataMasterContext::fromJson(std::string json) {
    std::shared_ptr<DataMasterContext> context = std::make_shared<DataMasterContext> ();

    return context;
}

std::string DataMasterContext::toJson() {

    std::string json;

    auto doc = std::make_shared<DocumentType>();

    return JsonUtil::docToString (doc.get());
}

DataMasterContext::~DataMasterContext() {

}
