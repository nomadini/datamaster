#ifndef DataMasterContext_H
#define DataMasterContext_H



class StringUtil;
#include <memory>
#include <string>
#include "Poco/Net/NameValueCollection.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "tbb/concurrent_hash_map.h"

class DataMasterContext;



class DataMasterContext {

private:


public:

DataMasterContext();

std::string toString();

static std::shared_ptr<DataMasterContext> fromJson(std::string json);

std::string toJson();

virtual ~DataMasterContext();
};

#endif
