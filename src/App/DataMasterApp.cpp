
#include "GUtil.h"
#include "CassandraDriverInterface.h"
#include "ConfigService.h"
#include "DataMaster.h"
#include "TempUtil.h"
#include "LogLevelManager.h"
#include <thread>
#include "GUtil.h"
#include "StringUtil.h"
#include "SignalHandler.h"
#include <string>
#include <memory>

#include <thread>
#include <mutex>  // For std::unique_lock
#include <boost/thread.hpp>
#include <thread>
#include <boost/foreach.hpp>
#include "BeanFactory.h"
#include "EntityToModuleStateStats.h"
#include "ConfigService.h"
#include "AtomicBoolean.h"
#include "DataMasterRequestHandlerFactory.h"
#include "CommonRequestHandlerFactory.h"
#include "DataMaster.h"
#include "DataReloadService.h"
#include "BeanFactory.h"
#include "ServiceFactory.h"
#include "ConcurrentHashMap.h"
int main(int argc, char* argv[]) {

        std::string appName = "DataMaster";
        TempUtil::configureLogging (appName, argv);
        std::string propertyFileName = "dataMaster.properties";
        auto beanFactory = std::make_unique<BeanFactory>("SNAPSHOT");
        beanFactory->propertyFileName = propertyFileName;
        beanFactory->commonPropertyFileName = "common.properties";
        beanFactory->appName = appName;
        beanFactory->initializeModules();

        auto serviceFactory = std::make_unique<ServiceFactory>(beanFactory.get());
        serviceFactory->initializeModules();


        LOG(INFO) << "google log starting the dataMaster : argc "<< argc << " argv : " <<  *argv;

        auto threadsInterruptedFlag = std::make_shared<gicapods::AtomicBoolean>();

        std::shared_ptr<DataMaster> dataMaster = std::make_shared<DataMaster>();
        dataMaster->threadsInterruptedFlag = threadsInterruptedFlag;

        std::string allClientsUrlsProperty = beanFactory->configService->get("allDataClientUrls");
        dataMaster->clientUrls = StringUtil::split (allClientsUrlsProperty, ",");
        assertAndThrow(!dataMaster->clientUrls.empty());
        dataMaster->dataMasterRequestHandlerFactory = serviceFactory->dataMasterRequestHandlerFactory.get();


        dataMaster->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        dataMaster->dataReloadService = serviceFactory->dataReloadService;
        dataMaster->entityToModuleStateStatsPersistenceService =
                beanFactory->entityToModuleStateStatsPersistenceService.get();
        dataMaster->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        dataMaster->configService = beanFactory->configService.get();
        dataMaster->propertyFileName = propertyFileName;
        dataMaster->run(argc, argv);

}
