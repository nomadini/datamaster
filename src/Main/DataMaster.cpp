
#include "GUtil.h"
#include "StringUtil.h"
#include "SignalHandler.h"
#include "Poco/Util/HelpFormatter.h"
#include "DataMaster.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/HTTPServer.h"
#include "CampaignCacheService.h"
#include "TargetGroupCacheService.h"
#include "ConfigService.h"
#include "ConverterUtil.h"
#include <boost/exception/all.hpp>
#include "EntityToModuleStateStats.h"

#include "NetworkUtil.h"
#include "LogLevelManager.h"

#include "TargetGroupBWListCacheService.h"
#include "MySqlTargetGroupService.h"
#include "MySqlGeoSegmentListService.h"
#include "MySqlGeoLocationService.h"
#include "MySqlTargetGroupDayPartTargetMapService.h"
#include "MySqlTargetGroupGeoSegmentListMapService.h"
#include "MySqlTargetGroupCreativeMapService.h"
#include "MySqlSegmentService.h"
#include "MySqlTargetGroupSegmentMapService.h"

#include "MySqlTargetGroupCreativeMapService.h"
#include "TargetGroupGeoSegmentCacheService.h"
#include "TargetGroupDayPartCacheService.h"
#include "MySqlTargetGroupDayPartTargetMapService.h"
#include "MySqlTargetGroupGeoLocationService.h"
#include "TargetGroupGeoLocationCacheService.h"
#include <thread>
#include "EntityDeliveryInfoCacheService.h"
#include "HttpUtilService.h"
#include "AerospikeDriver.h"
#include "CassandraDriverInterface.h"
#include "HttpUtilService.h"
#include "MySqlMetricService.h"
#include "GlobalWhiteListedCacheService.h"
#include "HttpUtil.h"
#include "PocoHttpServer.h"

DataMaster::DataMaster() {

}

DataMaster::~DataMaster() {

}

void DataMaster::initialize(Poco::Util::Application &self) {
        loadConfiguration (); // load default configuration files, if present
        Poco::Util::ServerApplication::initialize (self);
}

void DataMaster::uninitialize() {
        Poco::Util::ServerApplication::uninitialize ();
}


void DataMaster::runEveryOnceInAWhile() {
        while (true) {
                try {
                        entityToModuleStateStats->addStateModuleForEntity ("runEveryOnceInAWhile",
                                                                           "DataMaster",
                                                                           "ALL");
                        gicapods::Util::printMemoryUsage();
                } catch (std::exception const &e) {
                        //TODO : go red and show in dashboard that this is red
                        LOG(ERROR) << "error happening when runEveryOnceInAWhile  " << boost::diagnostic_information (e);
                        gicapods::Util::showStackTrace();
                        entityToModuleStateStats->
                        addStateModuleForEntity ("runEveryOnceInAWhile",
                                                 "DataMaster",
                                                 "ALL",
                                                 EntityToModuleStateStats::exception);
                }
                int interval;
                configService->get<int>("onceInAWhileFunctionsIntervalInSec", interval);
                gicapods::Util::sleepViaBoost (_L_, interval);
        }

}

void DataMaster::reloadCaches(std::shared_ptr<gicapods::AtomicBoolean> interrupted) {
        do {
                try{

                        MLOG(3) << "attempting to reloading the caches";

                        gicapods::Util::sleepViaBoost (_L_, 2);//sleeping for a second so that all biddings are done

                        dataReloadService->reloadData();

                } catch (std::exception const &e) {
                        LOG(ERROR) << "error happening when reloading caches  " << boost::diagnostic_information (e);
                        entityToModuleStateStats->
                        addStateModuleForEntity ("EXCEPTION_IN_RELAODING_CACHES",
                                                 "DataMaster",
                                                 "ALL",
                                                 EntityToModuleStateStats::exception);
                }

                if(interrupted->getValue()) {
                        break;
                }
                int reloadingCacheInterval = ConverterUtil::convertTo<int> (
                        configService->get ("reloadCachesIntervalInSec"));

                MLOG(3) << "done with reloading the caches, reloadingCacheInterval : "
                        << reloadingCacheInterval;
                gicapods::Util::sleepViaBoost (_L_, reloadingCacheInterval);
        } while (!interrupted->getValue());

}

int DataMaster::main(const std::vector<std::string> &args) {

        //reload caches once before starting to serve requests
        reloadCaches(std::make_shared<gicapods::AtomicBoolean>(true));
        MLOG(3) << "going to run daemon threads for dataMaster" << _L_;
        this->entityToModuleStateStatsPersistenceService->startThread();
        std::thread runEveryOnceInAWhileThread (&DataMaster::runEveryOnceInAWhile, this);
        runEveryOnceInAWhileThread.detach ();

        std::thread startReloadingCacheThread (&DataMaster::reloadCaches, this, threadsInterruptedFlag);
        startReloadingCacheThread.detach ();

        auto srv = PocoHttpServer::createHttpServer(configService, dataMasterRequestHandlerFactory);

        // start the HTTPServer
        srv->start ();
        // wait for CTRL-C or kill
        waitForTerminationRequest ();
        // Stop the HTTPServer
        srv->stop ();

        return Application::EXIT_OK;
}
