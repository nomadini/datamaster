#ifndef GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_H_
#define GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_H_


#include <memory>
#include <string>
#include "DataMasterRequestHandlerFactory.h"
#include "Poco/Util/Application.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/ServerApplication.h"
class TargetGroupCacheService;
class CampaignCacheService;
class CreativeCacheService;
class EntityToModuleStateStats;
#include "MySqlBWListService.h"

#include "MySqlTargetGroupBWListMapService.h"
#include "DataReloadService.h"
namespace gicapods { class ConfigService; }
#include "EntityToModuleStateStatsPersistenceService.h"
#include "AtomicBoolean.h"
#include "CacheService.h"
#include "Advertiser.h"
#include <boost/thread.hpp>
#include <thread>
class DataMaster;



class DataMaster : public std::enable_shared_from_this<DataMaster>, public Poco::Util::ServerApplication {

public:

std::vector<std::string> clientUrls;
std::shared_ptr<gicapods::AtomicBoolean> threadsInterruptedFlag;
gicapods::ConfigService* configService;
DataMasterRequestHandlerFactory* dataMasterRequestHandlerFactory;
TargetGroupCacheService* targetGroupCacheService;
CampaignCacheService* campaignCacheService;
CacheService<Advertiser>* advertiserCacheService;
CreativeCacheService* creativeCacheService;
EntityToModuleStateStats* entityToModuleStateStats;

MySqlBWListService* mySqlBWListService;
std::shared_ptr<MySqlTargetGroupBWListMapService> mySqlTargetGroupBWListMapService;
std::shared_ptr<DataReloadService> dataReloadService;
EntityToModuleStateStatsPersistenceService* entityToModuleStateStatsPersistenceService;
std::string propertyFileName;

DataMaster();

virtual ~DataMaster();

void initialize(Application &self);

void uninitialize();

void runEveryOnceInAWhile();

void reloadCaches(std::shared_ptr<gicapods::AtomicBoolean> interrupted);

int main(const std::vector<std::string> &args);

private:
bool _helpRequested;
};

#endif
